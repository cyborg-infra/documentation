#!/bin/bash

set -euo pipefail

exit_error() { echo -e "\033[0;31m$@\033[0m"; exit 1; }

TOOLS="git hugo npm"

for TOOL in $TOOLS; do
    command -v $TOOL &>/dev/null || \
        exit_error "Command '$TOOL' not found, required comands: $TOOLS"
done
