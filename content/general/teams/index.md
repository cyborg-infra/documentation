---
title: "Teams and scope"
linktitle: "Teams"
description: >
  Provide further information and pointers about the teams under the Cyborg
  umbrella
weight: 10
---

For the purposes of these documentation pages, the following teams fall under
the Cyborg umbrella:

| Team                     | Public documentation                                      |
|--------------------------|-----------------------------------------------------------|
| AMUSE                    |                                                           |
| Anaconda                 |                                                           |
| Automotive               |                                                           |
| [CKI][cki]               | [GitLab][cki-public], [Source][cki-public-source]         |
| Cockpit                  |                                                           |
| [Copr/CPT][copr]         | [Pagure][copr-public], [Source][copr-public-source]       |
| [Image Builder][osbuild] | [GitHub][osbuild-public], [Source][osbuild-public-source] |
| OSCI                     |                                                           |
| [Packit][packit]         | [Github][packit-public], [Source][packit-source]          |
| Testing Farm             | [GitLab][tf-public], [Source][tf-public-source]           |

{{% include "internal.md" %}}

[copr]: https://copr.fedorainfracloud.org/
[copr-public]: https://docs.pagure.org/copr.copr/maintenance_documentation.html
[copr-public-source]: https://pagure.io/copr/copr/blob/main/f/doc/maintenance_documentation.rst

[cki]: https://cki-project.org/
[cki-public]: https://cki-project.org/docs/
[cki-public-source]: https://gitlab.com/cki-project/documentation/

[osbuild]: https://www.osbuild.org/
[osbuild-public]: https://www.osbuild.org/guides/
[osbuild-public-source]: https://github.com/osbuild/guides/

[packit]: https://packit.dev
[packit-public]: https://packit.dev
[packit-source]: https://github.com/packit/packit.dev

[tf-public]: https://docs.testing-farm.io
[tf-public-source]: https://gitlab.com/testing-farm/docs/root
