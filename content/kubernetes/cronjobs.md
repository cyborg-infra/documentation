---
title: CronJobs
description: >
  How to correctly configure CronJobs on a Kubernetes cluster.
---

[CronJob Documentation]

## Problem

Correctly managing CronJobs in Kubernetes has various pitfalls. This page
documents the best practices for the Cyborg teams to conquer CronJobs
and mitigate suprises.

## spec

[CronJob spec Documentation]

### Set `schedule` reasonably according to your use-case

This will make sure your cronjobs run as often as needed. Note that running
cronjobs very often can elevate the pitfalls and also cause unneeded load
on the cluster. Be resonable and think about it. Make sure you understand
how long your cronjobs take, before running them very often.

### Set `concurrencyPolicy` to either `Forbid` or `Replace`

The default `concurrencyPolicy` policy is to `Allow` which can result
in multiple Jobs running at the same time. For jobs that sometimes might
take a bit longer than expected, use `Forbid` to skip successive launches
as long as a job is still running. For jobs that might hang, use `Replace`
so the old job is replaced with a new one.

### Set `successfulJobsHistoryLimit` to `2` and `failedJobsHistoryLimit` to `1`

Each CronJob will keep a certain number of finished Jobs around, which will
in turn keep a certain number of Pods. Limiting the number of successful
and failed Jobs that are kept that way will make sure you have a reasonable
history available, while not consuming too many pods in the namespace.

### Set `startingDeadlineSeconds` to some value, at least larger than 10s

See [this medium article] for a nice explanation about this option. See also
the possible [pitfalls of this option here]

## jobTemplate.spec

[jobTemplate.spec Documentation]

### Set `backoffLimit` to `1`

To mitigate retrying the job too many times, default setting is `6`.

## jobTemplate.spec.template.spec

[jobTemplate.spec.template.spec Documentation]

### Set `restartPolicy` to `Never`

If you do not want to restart failed pods, make sure to enable this option.
There is rarely a reason for that.

## jobTemplate.spec.template.spec.containers

[jobTemplate.spec.template.spec.containers Documentation]

### Set container resource requests and limits

Follow the [presentation] from Cyborg Infrastructure Meetup 2022.

## Example

Here is a reasonable example if you just want something to copy/paste.

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: my-cronjob
  labels:
    app: my-app
spec:
  schedule: "*/10 * * * *"
  startingDeadlineSeconds: 3600
  successfulJobsHistoryLimit: 2
  failedJobsHistoryLimit: 1
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      backoffLimit: 1
      template:
        metadata:
          labels:
            app: my-app
        spec:
          containers:
            - name: my-container
              image: fedora:latest
              command: ["sleep", "20s"]
              resources:
                limits:
                  cpu: 50m
                  memory: 120M
                requests:
                  cpu: 25m
                  memory: 60M
          restartPolicy: Never
```

[CronJob Documentation]: https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs
[CronJob spec Documentation]: https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/cron-job-v1/
[jobTemplate.spec Documentation]: https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/job-v1/#JobSpec
[jobTemplate.spec.template.spec Documentation]: https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#PodSpec
[jobTemplate.spec.template.spec.containers Documentation]: https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#Container
[presentation]: https://docs.google.com/presentation/u/0/d/1vndheCsPLCJR9pxrAJbgY-FGHlefXnwdaOyt1Fz8X_c
[pitfalls of this option here]: https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/#cron-job-limitations
[this medium article]: https://medium.com/@hengfeng/what-does-kubernetes-cronjobs-startingdeadlineseconds-exactly-mean-cc2117f9795f
