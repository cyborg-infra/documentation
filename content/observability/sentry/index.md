---
title: Sentry
description: >
  How to use Sentry error tracking in your applications.
---

![Sentry](sentry.png)

## Why?

Discover issues sooner than your users!

Sentry is an open-source error tracking system. Sentry makes it easy to log
errors from any application and provide a lot of context which can help
to understand the root cause. It is a valuable tool for every developer
and can greatly enhance visibility of the long tail of infrequent
issues happening in your stack *in production*.

Interested how it looks like? Check out the [Sentry Sandbox].

## Available Instances

Cyborg teams have two Sentry instances available.

### Public applications

For public applications, teams can use the [Sentry.io] instance. This
is an SaaS offering directly from the creators of Sentry. Thanks to the Packit
team, we have a free `red-hat-0p` organization which you can use for your
Red Hat affiliated open-source projects.

To obtain access, please contact the [Packit or Testing Farm teams][teams].

### Internal applications

A on-premise deployment of Sentry is available in case you would like to
use Sentry for internal Red Hat applications.

{{% include "internal.md" %}}

To obtain access, please contact the [Testing Farm team][teams].

## Installation

Sentry has great documentation and supports most of the used programming
languages and major frameworks. See [Sentry Docs] for more information how
to use Sentry with your stack.

[Sentry.io]: https://sentry.io/welcome/
[Sentry Docs]: https://docs.sentry.io
[Sentry Sandbox]: https://try.sentry-demo.com/

[teams]: ../../general/teams.md
