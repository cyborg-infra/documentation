---
title: 'CKI: automatically applying updates'
linkTitle: 'CKI: offline-upgrade'
description: >
  How the CKI project applies updates once a week on VMs and bare-metal
  machines without causing production outages
---

## Problem

For image-based deployments via container and VM images, keeping systems up to
date is mostly solved by periodically recreating these machines.

For bare-metal machines or environments that don't support image-based
deployments, (security) updates need to be periodically installed and applied.

The following steps document a way to do this via [offline-upgrade] for the case
where redundant machines are available for a certain task, i.e. where machines
are allowed to be offline on an individual basis.

This is in production use in the CKI project for all its Beaker machines and
AWS VMs. The relevant Ansible role can be found in [com/infrastructure].

## General idea

Machines are going to be updated once a week via [offline-upgrade]. Updates are
deterministically applied on different weekdays for different machines of a
group. This causes machines to be rebooted once a week. Monitoring is used to
alert when a machine is unhealthy after updates are applied.

## Steps

1. For each machine, an update weekday needs to be determined. This can either
   be done automatically based on some characteristic like network segment or
   availability zone, or manually via the Ansible inventory:

   - Automatically based on AZ:

     ```yaml
     # vars.yml
     dnf_automatic_az_weekday_mappings:
       a: Tue
       b: Wed
       c: Thu
       d: Fri
       default: Tue
     ```

     ```yaml
     # playbook.yml for an EC2 machine
     - ansible.builtin.set_fact:
         dnf_automatic_weekday: "{{
             dnf_automatic_az_weekday_mappings
             [placement.availability_zone | last] |
             default(dnf_automatic_az_weekday_mappings.default)
           }}"
     ```

   - Manually in the inventory:

     ```yaml
     # inventory.yml
     all:
       hosts:
         first-machine:
           dnf_automatic_weekday: Tue
         second-machine:
           dnf_automatic_weekday: Wed
     ```

   In general, Tuesday, Wednesday or Thursday are the best days for machines
   where a reboot is fast. For machines where draining might take longer than a
   day, days need to be selected that are far enough apart to ensure that there
   will always be only one machine going through an update at any time.

1. The [offline-upgrade] command is not available by default and needs to be
   installed via

   ```yaml
   # playbook.yml
   - ansible.builtin.package:
       name: python3-dnf-plugin-system-upgrade
       state: present
   ```

1. To install updates, a systemd timer, a systemd unit and the actual update
   script are needed. The following templates and tasks configure updates once
   a week on the given weekday. The update script will manually stop certain
   systemd services that should be stopped *before* the machine is rebooted for
   the updates. This is repeated in a loop as `systemctl stop` might fail if
   another `systemctl start` invocation happens in the background. An example
   of a service where this is needed is [gitlab-runner], which can be
   configured to wait a long time until all currently running jobs are
   finished.

   ```toml
   # templates/offline-upgrade-weekly.timer
   [Unit]
   Description=offline-upgrade-weekly timer
   Wants=network-online.target

   [Timer]
   # change time zone so enough people are online to see stuff break
   OnCalendar={{ dnf_automatic_weekday }} *-*-* 10:00 UTC
   RandomizedDelaySec=60m
   Persistent=true
   Unit=offline-upgrade.service

   [Install]
   WantedBy=timers.target
   ```

   ```toml
   # templates/offline-upgrade.service
   Description=offline-upgrade update installation
   After=network-online.target

   [Service]
   Type=oneshot
   Nice=19
   IOSchedulingClass=idle
   IOSchedulingPriority=7
   Environment="SERVICES_TO_STOP={{ services_to_stop }}"
   ExecStart=/usr/local/sbin/offline-upgrade.sh
   ```

   ```bash
   # templates/offline-upgrade.sh
   #!/usr/bin/bash

   set -euo pipefail

   dnf offline-upgrade download --assumeyes

   IFS=' ' read -ra services <<< "${SERVICES_TO_STOP:-}"
   for service in "${services[@]}"; do
       while ! systemctl stop "${service}"; do
           echo "Failed to stop ${service}, retrying"
       done
   done


   dnf offline-upgrade reboot || true

   # dnf offline-upgrade reboot should reboot the machine if updates are
   # available, but sometimes this does not happen; in any case, reboot the
   # machine so that the uptime check via Prometheus is happy
   reboot
   ```

   ```yaml
   # playbook.yml
   - ansible.builtin.template:
       src: "{{ item.dest | basename }}"
       dest: "{{ item.dest }}"
       mode: "{{ item.mode | default('0644') }}"
     loop:
       - {dest: /etc/systemd/system/offline-upgrade.service}
       - {dest: /etc/systemd/system/offline-upgrade-weekly.timer}
       - {dest: /usr/local/sbin/offline-upgrade.sh, mode: '0755'}

   - ansible.builtin.systemd:
       name: offline-upgrade-weekly.timer
       enabled: true
       state: started
       daemon_reload: true
   ```

1. To alert about machines not rebooting as expected, an alerting rule like
   this can be used:

   ```yaml
   - alert: Missed reboot cycle
     # machines should reboot within 1 hour
     expr: (node_time_seconds - node_boot_time_seconds) / 60 / 60 > 7 * 24 + 1
     for: 1m
     labels:
       severity: important
     annotations:
       summary: "System {{ $labels.instance }} is not rebooting as planned"
       description: >-
           The system behind {{ $labels.instance }} has not rebooted on the
           expected schedule. That might mean that hosted services might be
           unavailable. The machine has to be fixed before more machines of
           the same group enter their reboot cycle.
   ```

1. Finally, proper monitoring is required so that unhealthy machines can be
   detected and fixed promptly. This is left as an exercise for the reader 🤗.

[com/infrastructure]: https://gitlab.com/cki-project/infrastructure/-/tree/main/ansible/roles/dnf_automatic/
[offline-upgrade]: https://dnf-plugins-extras.readthedocs.io/en/latest/system-upgrade.html
[gitlab-runner]: https://docs.gitlab.com/runner/
