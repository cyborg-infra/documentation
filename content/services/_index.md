---
title: Open Source Services
description: Various service-related documentation, procedures and best practices
---

The [Operate First] community defines two [Minimum Open Source requirements for Services]:

> - All the service's code and assets necessary to operate the service are shared
>   under an open source license, and publicly accessible.
> - A public contributor can use the same workflow as a typical team member to
>   make a change to the service.

See below how these concepts are implemented by different teams.

[Operate First]: https://www.operate-first.cloud/
[Minimum Open Source requirements for Services]: https://www.operate-first.cloud/community/open-source-services.html
